//
//  FilterDatasource.swift
//  Cool Brushing
//
//  Created by Adrian Barbos on 14/03/2017.
//  Copyright © 2017 gomobi. All rights reserved.
//

import UIKit

typealias FilterCompletion = ( (_ firstImage: UIImage, _ secondImage: UIImage, _ thirdImage: UIImage) -> () )

class FilterDatasource {
    
    private let imagePrefix = "filters-"
    private let startingImageIndex = 1
    private let endingImageIndex = 10
    
    private var currentItemIndex: Int = 0
    private var items: [UIImage] = [UIImage]()
    
    private var selectedIndexes: [Int] = [0, 1, 2]
    private var selectedImages: [UIImage] = []
    
    init() {
        for i in self.startingImageIndex...self.endingImageIndex {
            if let image = UIImage(named: "\(self.imagePrefix)\(i)") {
                image.accessibilityIdentifier = "\(self.imagePrefix)\(i)"
                self.items.append(image)
            }
        }
    }
    
    func firstItem() -> UIImage {
        return self.items[self.selectedIndexes[0]]
    }
    
    func secondItem() -> UIImage {
        return self.items[self.selectedIndexes[1]]
    }
    
    func thirdItem() -> UIImage {
        return self.items[self.selectedIndexes[2]]
    }
    
    func getItems(completion: FilterCompletion) {
        completion(
            self.items[self.selectedIndexes[0]],
            self.items[self.selectedIndexes[1]],
            self.items[self.selectedIndexes[2]]
        )
    }
    
    func previousItem(completion: FilterCompletion) {
        
        var index = self.selectedIndexes[2] + 1
        if index > self.items.count - 1 {
            index = 0
        }
        
        self.selectedIndexes = [
            self.selectedIndexes[1],
            self.selectedIndexes[2],
            index
        ]
        
        completion(
            self.items[self.selectedIndexes[0]],
            self.items[self.selectedIndexes[1]],
            self.items[self.selectedIndexes[2]]
        )
    }
    
    func nextItem(completion: FilterCompletion) {
        
        var index = self.selectedIndexes[0] - 1
        if index < 0 {
            index = self.items.count - 1
        }
        
        self.selectedIndexes = [
            index,
            self.selectedIndexes[0],
            self.selectedIndexes[1]
        ]
        
        completion(
            self.items[self.selectedIndexes[0]],
            self.items[self.selectedIndexes[1]],
            self.items[self.selectedIndexes[2]]
        )
    }
    
}
