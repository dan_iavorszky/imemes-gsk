//
//  GalleryVC.swift
//  Cool Brushing
//
//  Created by Adrian Barbos on 13/03/2017.
//  Copyright © 2017 gomobi. All rights reserved.
//

import UIKit
import Photos

class GalleryVC: UIViewController {
    
    var images = [UIImage]()
    var cellSize: CGSize?
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var collectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView.contentInset = UIEdgeInsetsMake(0, 0, 20, 0)
        
        let size = (UIScreen.main.bounds.size.width - 70) / 4;
        self.cellSize = CGSize(width: size, height: size)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.images = []
        self.activityIndicator.startAnimating()
        self.collectionView.reloadData()
        
        if PHPhotoLibrary.authorizationStatus() != .authorized{
            // If there is no permission for photos, ask for it
            PHPhotoLibrary.requestAuthorization(requestAuthorizationHandler)
            return
        }
        
        ImageGalleryHandler.sharedInstance.fetchAll { (images) in
            self.images = images
            self.collectionView.reloadData()
            self.activityIndicator.stopAnimating()
        }
    }
    
    func requestAuthorizationHandler(status: PHAuthorizationStatus){
        if PHPhotoLibrary.authorizationStatus() == .authorized{
            // The user did authorize, so, pickVideo may be opened
            // Ensure pickVideo is called from the main thread to avoid GUI problems
                ImageGalleryHandler.sharedInstance.fetchAll { (images) in
                    self.images = images
                    self.collectionView.reloadData()
                    self.activityIndicator.stopAnimating()
                }
        }
    }
    
    @IBAction func back() {
        self.navigationController!.popViewController(animated: true)
    }

}


extension GalleryVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "galleryCell", for: indexPath)
        
        (cell.viewWithTag(102) as? UIImageView)?.image = self.images[indexPath.row]
        
        return cell
        
    }
    
}

extension GalleryVC: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.view.bringSubview(toFront: self.activityIndicator)
        self.activityIndicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        ImageGalleryHandler.sharedInstance.fetch(atIndex: indexPath.row) { (image) in
            if let img = image {
                self.activityIndicator.stopAnimating()
                self.view.sendSubview(toBack: self.activityIndicator)
                UIApplication.shared.endIgnoringInteractionEvents()
                
                let story = UIStoryboard(name: "Cropper", bundle: nil)
                let destination = story.instantiateViewController(withIdentifier: "CropperVC") as! CropperVC
                destination.titleLabelText = "Galerie telefon"
                destination.image = img
                self.navigationController?.pushViewController(destination, animated: true)
            }
        }
        
        
    }
}

extension GalleryVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return self.cellSize!;
    }
    
}








