//
//  SplashVC.swift
//  Cool Brushing
//
//  Created by Adrian Barbos on 13/03/2017.
//  Copyright © 2017 gomobi. All rights reserved.
//

import UIKit

class SplashVC: UIViewController {
    
    private var isFirstStart: Bool {
        get {
            if let isFirst = UserDefaults.standard.object(forKey: "isFirstStart") as? Bool {
                return isFirst
            }
            return true
        } set {
            UserDefaults.standard.set(newValue, forKey: "isFirstStart")
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if self.hasBeenPushedFromHomeVC() {
            self.addBackgroundTapRecognizer();
        } else {
            if self.isFirstStart {
                self.isFirstStart = false
                self.perform(#selector(self.close), with: nil, afterDelay: 8)
            } else {
                self.close()
            }
        }
    }
    
    @objc private func close() {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let destination = storyboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        self.navigationController?.pushViewController(destination, animated: false)
    }
    
    private func addBackgroundTapRecognizer() {
        let tap = UITapGestureRecognizer()
        tap.numberOfTapsRequired = 1
        tap.addTarget(self, action: #selector(self.pop))
        self.view.isUserInteractionEnabled = true
        self.view.addGestureRecognizer(tap)
    }
    
    @objc private func pop() {
        self.navigationController!.popViewController(animated: true)
    }
    
    private func hasBeenPushedFromHomeVC() -> Bool {
        if let viewControllers = self.navigationController?.viewControllers {
            for vc in viewControllers {
                if vc is HomeVC {
                    return true
                }
            }
        }
        
        return false;
    }

}
