//
//  TermsAndConditionsVC.swift
//  Cool Brushing
//
//  Created by Adrian Barbos on 13/03/2017.
//  Copyright © 2017 gomobi. All rights reserved.
//

import UIKit

class TermsAndConditionsVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    @IBAction func closeButtonTap() {
        self.dismiss(animated: false, completion: nil)
    }

}
