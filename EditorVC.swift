//
//  EditorVC.swift
//  Cool Brushing
//
//  Created by Adrian Barbos on 13/03/2017.
//  Copyright © 2017 gomobi. All rights reserved.
//

import UIKit

class EditorVC: UIViewController, ZDStickerViewDelegate {
    
    var selectedImage: UIImage?
    
    var copyDatasource = CopyDatasource()
    @IBOutlet weak var copyImageView: UIImageView!
    
    @IBOutlet weak var imageContainer: UIView!
    
    var filterDatasource = FilterDatasource()
    @IBOutlet weak var firstFilterImage: UIImageView!
    @IBOutlet weak var secondFilterImage: UIImageView!
    @IBOutlet weak var thirdFilterImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imageContainer.layer.contents = self.selectedImage!.cgImage
        self.setupCopyContainer()
        self.setupFilters()
    }
    
    @IBAction func back() {
        self.navigationController!.popViewController(animated: true)
    }
    
    @IBAction func home() {
        if let viewControllers = self.navigationController?.viewControllers {
            for viewController in viewControllers where viewController is HomeVC {
                self.navigationController!.popToViewController(viewController, animated: true)
            }
        }
    }
    
    let tapx = UITapGestureRecognizer()
    @IBAction func share() {
        
        self.toggleStickersEdit()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { // in half a second...
            // Share the Meme
            let image = self.imageContainer.toImage
            let shareItems = ["Cel mai cool spălat pe dinți #coolbrushing", image] as [Any]
            
            
            let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
            activityViewController.excludedActivityTypes = [UIActivityType.print, UIActivityType.postToWeibo, UIActivityType.copyToPasteboard, UIActivityType.addToReadingList, UIActivityType.postToVimeo]
            
            activityViewController.completionWithItemsHandler = { activity, success, items, error in
                
                self.tapx.numberOfTapsRequired = 1
                self.tapx.addTarget(self, action: #selector(self.test))
                self.imageContainer.isUserInteractionEnabled = true
                self.imageContainer.addGestureRecognizer(self.tapx)
            }
            
            
            // iPhone
            self.present(activityViewController, animated: true, completion: nil)
        }
    }
    
    @objc func test() {
        self.toggleStickersEdit(remove: false)
        self.imageContainer.removeGestureRecognizer(self.tapx)
    }
    
    private func toggleStickersEdit(remove: Bool = true, completion: (() -> ())? = nil) {
        for view in self.imageContainer.subviews where view is ZDStickerView {
                if remove {
                    (view as! ZDStickerView).hideDelHandle()
                    (view as! ZDStickerView).hideEditingHandles()
                } else {
                    (view as! ZDStickerView).showDelHandle()
                    (view as! ZDStickerView).showEditingHandles()
                }
        }
        
        if completion != nil {
            completion!()
        }
    }
    
    /**
     Setup copy container
    **/
    private func setupCopyContainer() {
        self.copyImageView.image = self.copyDatasource.firstItem()
        self.addCopyImageViewTapRecognizer()
    }
    
    private func addCopyImageViewTapRecognizer() {
        let tap = UITapGestureRecognizer()
        tap.numberOfTapsRequired = 1
        tap.addTarget(self, action: #selector(self.addCopy))
        
        self.copyImageView.isUserInteractionEnabled = true
        self.copyImageView.addGestureRecognizer(tap)
    }
    
    @objc private func addCopy() {
        self.addSticker(image: self.copyDatasource.activeItem())
    }
    
    @IBAction func nextCopyButtonTap() {
        self.copyImageView.image = self.copyDatasource.nextItem()
    }
    
    @IBAction func previousCopyButtonTap() {
        self.copyImageView.image = self.copyDatasource.previousItem()
    }
    
    /**
     Setup filters container
     **/
    private func setupFilters() {
        
        
        let tap1 = UITapGestureRecognizer()
        tap1.numberOfTapsRequired = 1
        tap1.addTarget(self, action: #selector(self.addFirstFilter))
        self.firstFilterImage.isUserInteractionEnabled = true
        self.firstFilterImage.addGestureRecognizer(tap1)
        
        let tap2 = UITapGestureRecognizer()
        tap2.numberOfTapsRequired = 1
        tap2.addTarget(self, action: #selector(self.addSecondFilter))
        self.secondFilterImage.isUserInteractionEnabled = true
        self.secondFilterImage.addGestureRecognizer(tap2)
        
        let tap3 = UITapGestureRecognizer()
        tap3.numberOfTapsRequired = 1
        tap3.addTarget(self, action: #selector(self.addThirdFilter))
        self.thirdFilterImage.isUserInteractionEnabled = true
        self.thirdFilterImage.addGestureRecognizer(tap3)
        
        self.filterDatasource.getItems { (firstImage, secondImage, thirdImage) in
            self.firstFilterImage.image = firstImage
            self.secondFilterImage.image = secondImage
            self.thirdFilterImage.image = thirdImage
            
            self.negociateFilterImageBackgroundColor(firstImage: firstImage, secondImage: secondImage, thirdImage: thirdImage)
        }
    }
    
    @objc private func negociateFilterImageBackgroundColor(firstImage: UIImage, secondImage: UIImage, thirdImage: UIImage) {
        
        let identifier = firstImage.accessibilityIdentifier!
        if identifier == "filters-7" || identifier == "filters-8" {
            self.firstFilterImage.backgroundColor = UIColor(colorLiteralRed: 251/255, green: 189/255, blue: 12/255, alpha: 1)
        } else {
            self.firstFilterImage.backgroundColor = UIColor.white
        }
        
        let identifier2 = secondImage.accessibilityIdentifier!
        if identifier2 == "filters-7" || identifier2 == "filters-8" {
            self.secondFilterImage.backgroundColor = UIColor(colorLiteralRed: 251/255, green: 189/255, blue: 12/255, alpha: 1)
        } else {
            self.secondFilterImage.backgroundColor = UIColor.white
        }
        
        let identifier3 = thirdImage.accessibilityIdentifier!
        if identifier3 == "filters-7" || identifier3 == "filters-8" {
            self.thirdFilterImage.backgroundColor = UIColor(colorLiteralRed: 251/255, green: 189/255, blue: 12/255, alpha: 1)
        } else {
            self.thirdFilterImage.backgroundColor = UIColor.white
        }
        
    }
    
    @objc private func addFirstFilter() {
        self.addSticker(image: self.filterDatasource.firstItem(), isCopy: false)
    }
    
    @objc private func addSecondFilter() {
        self.addSticker(image: self.filterDatasource.secondItem(), isCopy: false)
    }
    
    @objc private func addThirdFilter() {
        self.addSticker(image: self.filterDatasource.thirdItem(), isCopy: false)
    }
    
    @IBAction func nextFilterButtonTap() {
        self.filterDatasource.nextItem { (firstImage, secondImage, thirdImage) in
            self.firstFilterImage.image = firstImage
            self.secondFilterImage.image = secondImage
            self.thirdFilterImage.image = thirdImage
            
            self.negociateFilterImageBackgroundColor(firstImage: firstImage, secondImage: secondImage, thirdImage: thirdImage)
        }
    }
    
    @IBAction func previousFilterButtonTap() {
        self.filterDatasource.previousItem { (firstImage, secondImage, thirdImage) in
            self.firstFilterImage.image = firstImage
            self.secondFilterImage.image = secondImage
            self.thirdFilterImage.image = thirdImage
            
            self.negociateFilterImageBackgroundColor(firstImage: firstImage, secondImage: secondImage, thirdImage: thirdImage)
        }
    }
}

extension EditorVC {
    
    internal func addSticker(image: UIImage, isCopy: Bool = true) {
        
        var size = CGSize.zero
        let imageSize = image.size
        let width = isCopy ? self.imageContainer.frame.size.width * 0.8 : self.imageContainer.frame.size.width * 0.5
        if imageSize.height > imageSize.width {
            size = CGSize(width: width, height: width * (imageSize.width / imageSize.height))
        } else if(imageSize.height < imageSize.width) {
            size = CGSize(width: width, height: width * (imageSize.height / imageSize.width))
        } else {
            size = CGSize(width: width, height: width)
        }
        
        let iv = UIImageView(frame:  CGRect(x: 40, y: 40, width: size.width, height: size.height))
        iv.image = image
        iv.clipsToBounds = true
        iv.contentMode = .scaleAspectFit
        
        let test = ZDStickerView(frame: CGRect(x: 40, y: 40, width: size.width, height: size.height))
        test.stickerViewDelegate = self
        test.contentView = iv
        test.preventsPositionOutsideSuperview = false
        test.showEditingHandles()
        //        test.translucencySticker = false
        test.showDelHandle()
        self.imageContainer.addSubview(test)
    }
    
}
