//
//  HomeVC.swift
//  Cool Brushing
//
//  Created by Adrian Barbos on 13/03/2017.
//  Copyright © 2017 gomobi. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {
    
    
    @IBOutlet weak var backgroundsButton: UIButton!
    @IBOutlet weak var galleryButton: UIButton!
    @IBOutlet weak var cameraButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    @IBAction func backgroundsButtonTap() {
        let story = UIStoryboard(name: "Backgrounds", bundle: nil)
        let destination = story.instantiateViewController(withIdentifier: "BackgroundsVC") as! BackgroundsVC
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    @IBAction func galleryButtonTap() {
        let story = UIStoryboard(name: "Gallery", bundle: nil)
        let destination = story.instantiateViewController(withIdentifier: "GalleryVC") as! GalleryVC
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    let camera = UIImagePickerController()
    @IBAction func cameraButtonTap() {
        camera.delegate = self
        camera.sourceType = .camera
        camera.cameraDevice = .front
        self.present(camera, animated: false, completion: nil)
    }
    
    @IBAction func tutorialButtonTap() {
        let story = UIStoryboard(name: "Splash", bundle: nil)
        let destination = story.instantiateViewController(withIdentifier: "SplashVC") as! SplashVC
        self.navigationController?.pushViewController(destination
            , animated: true)
    }

    @IBAction func termsAndConditionsButtonTap() {
        
        let story = UIStoryboard(name: "TermsAndConditions", bundle: nil)
        let destination = story.instantiateViewController(withIdentifier: "TermsAndConditionsVC") as! TermsAndConditionsVC
        destination.modalPresentationStyle = .overCurrentContext
        self.present(destination, animated: false, completion: nil)
        
    }
}

extension HomeVC {
    internal func setup() {
        self.styleHomeButton(button: self.backgroundsButton)
        self.styleHomeButton(button: self.galleryButton)
        self.styleHomeButton(button: self.cameraButton)
    }
    
    private func styleHomeButton(button: UIButton) {
        button.layer.borderWidth = 2.5
        button.layer.borderColor = UIColor.white.cgColor
        button.layer.cornerRadius = 25
    }
}

extension HomeVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let story = UIStoryboard(name: "Cropper", bundle: nil)
            let destination = story.instantiateViewController(withIdentifier: "CropperVC") as! CropperVC
            destination.image = pickedImage
            destination.delegate = self
            
            self.dismiss(animated: false, completion: { 
                self.navigationController?.pushViewController(destination, animated: true)
            })
        }
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: false, completion: nil)
    }
    
}

extension HomeVC: CropperProtocol {
    func retakePhoto() {
        self.cameraButtonTap()
    }
}
