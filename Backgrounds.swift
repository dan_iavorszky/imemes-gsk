//
//  Backgrounds.swift
//  Cool Brushing
//
//  Created by Adrian Barbos on 13/03/2017.
//  Copyright © 2017 gomobi. All rights reserved.
//

import UIKit

class Background {
    var title: String
    var image: UIImage
    
    init(title: String, image: UIImage) {
        self.title = title
        self.image = image
    }
}

class Backgrounds {
    
    var items: [Background] = [Background]()
    
    init() {
        self.items = [
            Background(title: "You don't say", image: UIImage(named: "1")!),
            Background(title: "I see", image: UIImage(named: "2")!),
            Background(title: "Mother of god", image: UIImage(named: "3")!),
            Background(title: "Challenge accepted", image: UIImage(named: "4")!),
            Background(title: "Okay", image: UIImage(named: "5")!),
            Background(title: "Face palm", image: UIImage(named: "6")!),
            Background(title: "All the things", image: UIImage(named: "7")!),
            Background(title: "I see what you did", image: UIImage(named: "8")!),
            Background(title: "Computer guy", image: UIImage(named: "9")!),
            Background(title: "Like a sir", image: UIImage(named: "10")!),
        ]
    }
    
    func atIndex(index: Int) -> Background {
        return self.items[index]
    }
    
    func total() -> Int {
        return self.items.count
    }
    
}
