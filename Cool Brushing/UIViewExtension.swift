//
//  UIViewExtension.swift
//  Cool Brushing
//
//  Created by Adrian Barbos on 14/03/2017.
//  Copyright © 2017 gomobi. All rights reserved.
//

import Foundation

extension UIView {
    
    var toImage: UIImage {
        let rect:CGRect = self.bounds
        UIGraphicsBeginImageContextWithOptions(rect.size, true, 0.0)
        self.drawHierarchy(in: self.bounds, afterScreenUpdates: false)
        let combinedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return combinedImage!
    }
    
}
