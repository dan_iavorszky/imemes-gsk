//
//  CropperVC.swift
//  Cool Brushing
//
//  Created by Adrian Barbos on 13/03/2017.
//  Copyright © 2017 gomobi. All rights reserved.
//

import UIKit

protocol CropperProtocol: class {
    func retakePhoto()
}

class CropperVC: UIViewController {
    
    var titleLabelText: String = "Camera"
    var image: UIImage?
    var delegate: CropperProtocol?
    @IBOutlet weak var titleLabel: UILabel!
    
    var scrollView : UIScrollView!
    var imageView : UIImageView!
    @IBOutlet weak var cropperContainer: UIView!
    
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.initCropper()
    }
    
    @IBAction func home() {
        if let viewControllers = self.navigationController?.viewControllers {
            for viewController in viewControllers where viewController is HomeVC {
                self.navigationController!.popToViewController(viewController, animated: true)
            }
        }
    }
    
    @IBAction func back() {
        if let delegate = self.delegate {
            delegate.retakePhoto()
        }
        
        self.navigationController!.popViewController(animated: true)
    }
    
    @IBAction func confirm() {
        
        let story = UIStoryboard(name: "Editor", bundle: nil)
        let destination = story.instantiateViewController(withIdentifier: "EditorVC") as! EditorVC
        destination.selectedImage = self.cropperContainer.toImage
        self.navigationController?.pushViewController(destination, animated: true)
        
    }

}

extension CropperVC: UIScrollViewDelegate {
    internal func initCropper() {
        
        guard let image = self.image else { return }
        
        scrollView=UIScrollView()
        scrollView.frame = CGRect(x: 0, y: 0, width: self.cropperContainer.frame.width, height: self.cropperContainer.frame.height)
        scrollView.minimumZoomScale=1
        scrollView.maximumZoomScale=3
        scrollView.bounces=false
        scrollView.delegate=self
        self.cropperContainer.addSubview(scrollView)
        
        imageView=UIImageView()
        imageView.image = image
        imageView.frame = CGRect(x: 0, y: 0, width: scrollView.bounds.width, height: scrollView.bounds.height)
        imageView.backgroundColor = UIColor.black
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        scrollView.addSubview(imageView)
    }
    
    //    private func rectForImage(image: UIImage) -> CGRect {
    //
    //        let imageSize = image.size
    //
    //        if imageSize.height > imageSize.width {
    //
    //            print("aici 1")
    //
    //            return CGRect(x: 0, y: 0, width: self.cropperContainer.frame.width, height: self.cropperContainer.frame.height * self.cropperContainer.frame.width / self.cropperContainer.frame.height)
    //        }
    //
    //        if (imageSize.height < imageSize.width) {
    //
    //            print("aici 2")
    //
    //            return CGRect(x: 0, y: 0, width: self.cropperContainer.frame.width * self.cropperContainer.frame.height / self.cropperContainer.frame.width, height: self.cropperContainer.frame.height)
    //        }
    //
    //        print("aici 3")
    //
    //        return CGRect(x: 0, y: 0, width: self.cropperContainer.frame.width, height: self.cropperContainer.frame.height)
    //    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
}

extension CropperVC {
    internal func setup() {
        self.titleLabel.text = self.titleLabelText
        
        self.styleButton(button: self.confirmButton)
        self.styleButton(button: self.cancelButton)
    }
    
    private func styleButton(button: UIButton) {
        button.layer.borderWidth = 2.5
        button.layer.borderColor = UIColor.white.cgColor
        button.layer.cornerRadius = 25
    }
}
