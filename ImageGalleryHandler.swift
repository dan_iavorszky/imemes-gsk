//
//  ImageGalleryHandler.swift
//  Cool Brushing
//
//  Created by Adrian Barbos on 13/03/2017.
//  Copyright © 2017 gomobi. All rights reserved.
//

import UIKit
import Photos

typealias FetchCompletion = ((_ image: UIImage?) -> ())
typealias FetchAllCompletion = ((_ images: [UIImage]) -> ())

class ImageGalleryHandler {
    
    static let sharedInstance = ImageGalleryHandler()
    
    internal let imgManager = PHImageManager.default()
    
    internal var requestOptions: PHImageRequestOptions {
        let requestOptions = PHImageRequestOptions()
        requestOptions.isSynchronous = true
        requestOptions.deliveryMode = .highQualityFormat
        return requestOptions
    }
    
    internal var fetchOptions: PHFetchOptions {
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key:"creationDate", ascending: false)]
        return fetchOptions
    }
    
    internal var size: CGSize {
        let size = (UIScreen.main.bounds.size.width - 70) / 4;
        return CGSize(width: size, height: size)
    }
    
    private init() {}
    
    func fetch(atIndex index: Int, completion: FetchCompletion) {
        self.fetchMethod(atIndex: index, completion: completion)
    }
    
    private func fetchMethod(atIndex index: Int, completion: FetchCompletion) {
        var foundImage: UIImage?
        
        let fetchResults: PHFetchResult = PHAsset.fetchAssets(with: .image, options: fetchOptions)
        
        if fetchResults.count > 0 {
            for i in 0..<fetchResults.count {
                if(i == index) {
                    imgManager.requestImage(for: fetchResults.object(at: i), targetSize: PHImageManagerMaximumSize, contentMode: PHImageContentMode.default, options: requestOptions, resultHandler: { (image, error) in
                        foundImage = image
                    })
                }
            }
        } else {
            print("You have no images")
        }
        
        completion(foundImage)
    }
    
    func fetchAll(completion: FetchAllCompletion) {
        self.fetchAllMethod(completion: completion)
    }
    
    private var triedOnce = false
    private func fetchAllMethod(completion: FetchAllCompletion) {
        var images = [UIImage]()
        
        let fetchResults: PHFetchResult = PHAsset.fetchAssets(with: .image, options: fetchOptions)
        
        if fetchResults.count > 0 {
            for i in 0..<fetchResults.count {
                imgManager.requestImage(for: fetchResults.object(at: i), targetSize: self.size, contentMode: .aspectFill, options: requestOptions, resultHandler: { (image, error) in
                    if let img = image {
                        images.append(img)
                    }
                })
            }
        } else {
            print("You have no images")
        }
        
        if(images.count == 0 && !self.triedOnce) {
            self.triedOnce = true
            self.fetchAllMethod(completion: completion)
        } else {
            completion(images)
        }
        
        
    }
    
}
