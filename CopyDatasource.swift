//
//  CopyDatasource.swift
//  Cool Brushing
//
//  Created by Adrian Barbos on 14/03/2017.
//  Copyright © 2017 gomobi. All rights reserved.
//

import UIKit

class CopyDatasource {
    
    private let imagePrefix = "copy-"
    private let startingImageIndex = 1
    private let endingImageIndex = 30
    
    private var currentItemIndex: Int = 0
    private var items: [UIImage] = [UIImage]()
    
    init() {
        for i in self.startingImageIndex...self.endingImageIndex {
            if let image = UIImage(named: "\(self.imagePrefix)\(i)") {
                self.items.append(image)
            }
        }
    }
    
    func activeItem() -> UIImage {
        return self.items[self.currentItemIndex]
    }
    
    func firstItem() -> UIImage {
        self.currentItemIndex = 0
        return self.items[0]
    }
    
    func lastItem() -> UIImage {
        self.currentItemIndex = self.items.count - 1
        return self.items[self.currentItemIndex]
    }
    
    func nextItem() -> UIImage {
        self.currentItemIndex += 1
        if self.currentItemIndex <= self.items.count - 1 {
            return self.items[self.currentItemIndex]
        }
        
        return self.firstItem()
    }
    
    func previousItem() -> UIImage {
        
        if self.currentItemIndex == 0 {
            return self.lastItem()
        }
        
        self.currentItemIndex -= 1
        return self.items[self.currentItemIndex]
    }
    
}
