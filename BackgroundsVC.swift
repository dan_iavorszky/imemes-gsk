//
//  BackgroundsVC.swift
//  Cool Brushing
//
//  Created by Adrian Barbos on 13/03/2017.
//  Copyright © 2017 gomobi. All rights reserved.
//

import UIKit

class BackgroundsVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var backgrounds = Backgrounds()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    @IBAction func back() {
        self.navigationController!.popViewController(animated: true)
    }
}

extension BackgroundsVC {
    internal func setup() {
        self.tableView.estimatedRowHeight = 50
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 10, 0)
    }
}

extension BackgroundsVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.backgrounds.total()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "backgroundCell")!
        cell.backgroundColor = UIColor.clear
        
        let background = self.backgrounds.atIndex(index: indexPath.row)
        (cell.viewWithTag(100) as? UIImageView)?.image = background.image
        (cell.viewWithTag(101) as? UILabel)?.text = background.title
    
        
        return cell
    }
}

extension BackgroundsVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let story = UIStoryboard(name: "Editor", bundle: nil)
        let destination = story.instantiateViewController(withIdentifier: "EditorVC") as! EditorVC
        destination.selectedImage = self.backgrounds.atIndex(index: indexPath.row).image
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
}
